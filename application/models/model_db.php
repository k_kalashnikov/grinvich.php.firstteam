<?php

    define('MYSQL_SERVER', 'localhost');
    define('MYSQL_USER', 'root');
    define('MYSQL_PASSWORD', '');
    define('MYSQL_DB', 'meet');

	function connectDB() {
          return  $db = new mysqli (MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB);
    }

    function closeDB($db){
        $db->close();
    }

?>