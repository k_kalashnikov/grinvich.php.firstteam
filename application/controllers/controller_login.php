<?
session_start();
class Controller_login extends Controller{

    function action_index()
	{
        $this->view->generate('view_login.php', 'view_template.php', 0);
	}
    public function action_in()
    {
         require_once "application/models/model_db.php";
         require_once "application/models/model_user.php";
         $login = $_POST ['login'];
         $password = ($_POST['password']);
         unset($_SESSION['error']);

         if (checkLogin($login, $password)){
            $user = userInf($login);
            $_SESSION['id'] = $user['id'];
            $_SESSION['login'] = $user['login'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['s_name'] = $user['s_name'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['birthday'] = $user['birthday'];
            $_SESSION['country'] = $user['country'];
            $_SESSION['city'] = $user['city'];
            header("Location: /");
         }
         else 
         {
             $_SESSION['error'] = 1;
             header("Location: /login");
         }

    }

}


