<?php
session_start();
class Controller_edit extends Controller{

    function action_index()
	{
        $this->view->generate('view_edit.php', 'view_template.php', 0);
	}
     public function action_up()
    {
            require_once "application/models/model_db.php";
            require_once "application/models/model_user.php";
            if (isset($_POST['edit'])){
            $login = $_POST['login'];
            $email = $_POST['email'];
            $name = $_POST['name'];
            $s_name = $_POST['s_name'];
            $password = $_POST['password'];
            $birthday = $_POST['birthday'];
            $country = $_POST['country'];
            $city = $_POST['city'];
            $badeditlogin = false;
            $badeditname = false;
            $badeditsname = false;
            $badeditemail = false;
            $badeditcountry = false;
            $badeditcity = false;
            $badeditbrithday = false;
            $badeditpassword = false;
            if ($login != $_SESSION['login']){
                if ((checkLogin1($login)== false) || (strlen($login) < 3 ) || (strlen($login) >32 )){
                $_SESSION['erroredit_login']= 1;
                if ($login == ""){
                    $_SESSION['erroredit_login']= 0;
                }
                $badeditlogin = true;                
                header("Location: /edit");
                }
                if ((!$badeditlogin) &&($login !="")) {
                editLogin($login);  
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            if ($name != $_SESSION['name']){
                if ((strlen($name) < 2 ) || (strlen($name) >32 )) { 
                $_SESSION['erroredit_name']= 1;
                if ($name == ""){
                 $_SESSION['erroredit_name']= 0;   
                }
                $badeditname = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditname) && ($name != "")) {
                editName($name);
                reset($_SESSION['name']);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
             if ($email != $_SESSION['email']){
                if (checkEmail($email)== false) { 
                $_SESSION['erroredit_email']= 1;
                if ($email == ""){
                 $_SESSION['erroredit_email']= 0;   
                }
                $badeditemail = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditemail) && ($email != "")) {
                editEmail($email);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            if ($country != $user['country']){
                if ((strlen($country) < 2 ) || (strlen($country) >32 )) { 
                $_SESSION['erroredit_country']= 1;
                if ($country == ""){
                 $_SESSION['erroredit_country']= 0;   
                }
                $badeditcountry = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditcountry) && ($country != "")) {
                editCountry($country);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            
             if ($s_name != $user['s_name']){
                if ((strlen($s_name) < 2 ) || (strlen($s_name) >32 )) { 
                $_SESSION['erroredit_s_name']= 1;
                if ($s_name == ""){
                 $_SESSION['erroredit_s_name']= 0;   
                }
                $badeditsname = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditsname) && ($s_name != "")) {
                editS_Name($s_name);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            if ($city != $_SESSION['city']){
                if (strlen($city) >32 ) { 
                $_SESSION['erroredit_city']= 1;
                if ($city == ""){
                 $_SESSION['erroredit_city']= 0;   
                }
                $badeditcity = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditcity) && ($city !="")) {
                editCity($city);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            
                if ($birthday != $_SESSION['birthday']){ 
                if ((!$badeditbrithday) && ($birthday != "")) {
                editBirthday($birthday);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                    }
                }

                 if ($city != $_SESSION['city']){
                if (strlen($city) >32 ) { 
                $_SESSION['erroredit_city']= 1;
                if ($city == ""){
                 $_SESSION['erroredit_city']= 0;   
                }
                $badeditcity = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditcity) && ($city !="")) {
                editCity($city);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            if ($password != $_SESSION['password']){
                if ((strlen($password) < 6 ) || (strlen($password) >32 )) { 
                $_SESSION['erroredit_password']= 1;
                if ($password == ""){
                 $_SESSION['erroredit_password']= 0;   
                }
                $badeditpassword = true;                
                header("Location: /edit");
                    }   
                if ((!$badeditpassword) && ($password !="")) {
                editPassword($password);
                $_SESSION['edit_success'] = 1;
                header("Location: /edit");
                }
            }
            
         }  
    }
}
