<?
session_start();
class controller_reg extends Controller
{
    public function action_index()
	{
        $this->view->generate('view_reg.php', 'view_template.php', 0);
    }
    public function action_in()
    {
        require_once "application/models/model_db.php";
        require_once "application/models/model_user.php";
            if (isset($_POST['reg1'])){
            $login = $_POST['login'];
            $email = $_POST['email'];
            $name = $_POST['name'];
            $s_name = $_POST['s_name'];
            $password = ($_POST['password']);
            $bad = false;
            unset ($_SESSION['error_login']);
            unset ($_SESSION['error_password']);
            unset ($_SESSION['reg_success']);
            unset ($_SESSION['error_email']);
            unset ($_SESSION['error_login1']);
            unset ($_SESSION['empty_email']);
            unset ($_SESSION['empty_name']);
            unset ($_SESSION['empty_s_name']);
            if (empty($email)){
                $_SESSION['empty_email'] = 1;
                $bad = true;
                header ("Location: /reg");
            }
            if (empty($name)){
                $_SESSION['empty_name'] = 1;
                $bad = true;
                header ("Location: /reg");
            }
            if (empty($s_name)){
                $_SESSION['empty_s_name'] = 1;
                $bad = true;
                header ("Location: /reg");
            }
            if ((strlen($login) < 3 ) || (strlen($login) >32 )) { 
                $_SESSION['error_login']= 1;
                $bad = true;                
                header("Location: /reg");
            }
            if ((strlen ($password) < 6 ) || (strlen($password) >32 )) {
                $_SESSION['error_password']= 1;
                $bad = true;
                header("Location: /reg");
            }
            if  (checkEmail($email) == false){
                $_SESSION ['error_email'] = 1;
                $bad = true;
                header ("Location: /reg");
            }
            if  (checkLogin1($login) == false){
                $_SESSION ['error_login1'] = 1;
                $bad = true;
                header ("Location: /reg");
            }
            if (!$bad) {
                regUser($login, $email, $name, $s_name, $password);
                $_SESSION['reg_success'] = 1;
                header("Location: /login");
            }
            else{
                header("Location: /reg");   
            }
        }
        else
        {
             header("Location: /reg"); 
        }
    }
}
?>