<?php
session_start();
class Controller_logout extends Controller{

    function action_index()
	{
        $this->view->generate('view_logout.php', 'view_template.php', 0);
	}
    public function action_out()
    {  
        unset ($_SESSION['login']);
        unset ($_SESSION['password']);
        unset ($_SESSION['name']);
        unset ($_SESSION['s_name']);
        unset ($_SESSION['email']);
        unset ($_SESSION['birthday']);
        unset ($_SESSION['city']);
        unset ($_SESSION['country']);
        unset ($_SESSION['id']);
        header ('Location: /');
            }
}
?>