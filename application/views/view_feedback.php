<!DOCTYPE html>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form class="form-horizontal form-feedback" id ="form1" action = "request.php" method = "POST">
                    <fieldset>
                        <legend>Обратная связь</legend>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email: </label>
                            <div class="col-lg-10">
                                <input type = 'email' name = 'email' class="form-control" id="inputEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Сообщение: </label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="textArea" name = 'message'></textarea>
                                <?php
                                    if($_GET['mail'] == 1) echo "<p style = 'color:red;'> Сообщение отправлено</p>";
                                    else echo "<span class='help-block'>Мы вам скоро ответим...</span>";
                                ?>  
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-danger">Отправить</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>         

</body>
</html>