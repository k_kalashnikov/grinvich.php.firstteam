  <div class="container">
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <form class="form-horizontal form-exit" method='post' action='login/in' id='login' >
                <?php
                
                if($_SESSION['reg_success'] == 1) echo "<p><span style ='color: green'>Вы зарегистрированы!</span></p>";
                if ($_SESSION['error'] == 1) echo "<p><span style ='color: red'>Неправильный логин или пароль</span></p>";
                unset($_SESSION['error']); 
                unset($_SESSION['reg_success']);
                ?>
                    <fieldset>
                        <legend>Вход</legend>
                        <div class="form-group">
                            <div class="login-space">
                                <label for="inputLog" class="col-lg-2 control-label">Логин: </label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="login" placeholder="">
                                </div>
                            </div>
                            <div class="password-space">
                                <label for="inputPass" class="col-lg-2 control-label">Пароль: </label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" name="password" placeholder="">
                                </div>
                            </div>
                            
                        </div>            
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" name='enter' class="btn btn-danger">Войти</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
