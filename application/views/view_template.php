<?php
session_start();
?>
<!DOCTYPE html>

<head>

    <meta charset="utf-8">

    <title>Сайт знакомств</title>


    <link rel="stylesheet" type="text/css" href="css/bootstrap_main.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap_skin.css">
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/fonts.css">
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="libs/bootstrap/js/bootstrap.js"></script>


</head>

<body>

	<nav class="navbar navbar-inverse">
    	<div class="container-fluid">
        	<div class="navbar-header">
            	<a class="navbar-brand" href="/">Сайт знакомств</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
        	</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	            	<ul class="nav navbar-nav">
		             	<?php
						 if ($_SESSION['name']!== NULL){
						 echo "<li><a href='profile'>Профиль</a></li>";
						 echo "<li><a href='private'>Приватный Чат</a></li>";
						 }
						 ?>
            		</ul>
            
            		<ul class="nav navbar-nav navbar-right">
						<li><a href="feedback">Обратная связь</a></li>
						<li><a href="reg">Регистрация</a></li>
						<?php
						if ($_SESSION['name'] == NULL){		
						echo "<li><a href='login'>Вход</a></li>";
						}
						else{
						echo "<li><a href='logout/out'>Выход</a></li>";	
						}
						?>
            		</ul>
        		</div>
    	</div>
	</nav>


    


</body>
</html>