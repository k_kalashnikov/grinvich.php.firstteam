    <div class="container">
        <div class="row">
            <div class="col-md-4">
                
            </div>
            <div class="col-md-5">
                    <?php            
                    if($_SESSION ['erroredit_login']== 1) echo "<p><span style = 'color: red;'>Неккорентный Логин</span></p>";
                    if($_SESSION ['erroredit_name']== 1) echo "<p><span style = 'color: red;'>Неккорентное Имя</span></p>";
                    if($_SESSION ['erroredit_email']== 1) echo "<p><span style = 'color: red;'>Неккорентный Email</span></p>"; 
                    if($_SESSION ['erroredit_s_name']== 1) echo "<p><span style = 'color: red;'>Неккорентная Фамилия</span></p>";
                    if($_SESSION ['erroredit_city']== 1) echo "<p><span style = 'color: red;'>Неккорентный город</span></p>"; 
                    if($_SESSION ['erroredit_country']== 1) echo "<p><span style = 'color: red;'>Неккорентная Страна</span></p>";
                    if($_SESSION ['erroredit_password']== 1) echo "<p><span style = 'color: red;'>Неккорентный Пароль</span></p>";   
                    if($_SESSION ['edit_success']== 1) echo "<p><span style = 'color: green;'>Изменение успешно сохранены! </span></p>";
                    unset ($_SESSION['erroredit_login']);
                    unset ($_SESSION['erroredit_name']);
                    unset ($_SESSION['erroredit_email']);
                    unset ($_SESSION['erroredit_country']);
                    unset ($_SESSION['erroredit_s_name']);
                    unset ($_SESSION['erroredit_city']);
                    unset ($_SESSION['erroredit_password']);
                    unset ($_SESSION['edit_success']); 
                    ?>
                <form class="form-horizontal form-exit" id = 'form' action='edit/up' method='POST' >
                    <legend>Редактирование профиля</legend>
                        <div class="form-group">
                            <div class="login-space exit-input">
                                <label for="inputLog" class="col-lg-2 control-label">Логин:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="login" placeholder="">
                                </div>
                            </div>
                            <div class="email-space exit-input">
                                <label for="inputLog" class="col-lg-2 control-label">E-mail:</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" name="email" placeholder="">
                                </div>
                            </div>
                            <div class="name-space exit-input">
                                <label for="inputname" class="col-lg-2 control-label">Имя:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="name" placeholder="">
                                </div>
                            </div>
                            <div class="s_name-space exit-input">
                                <label for="inputs_name" class="col-lg-2 control-label">Фамилия:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="s_name" placeholder="">
                                </div>
                            </div>
                            <div class="country-space exit-input">
                                <label for="inputContry" class="col-lg-2 control-label">Страна:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="country" placeholder="">
                                </div>
                            </div>
                            <div class="city-space exit-input">
                                <label for="inputCity" class="col-lg-2 control-label">Город:</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="city" placeholder="">
                                </div>
                            </div>
                            <div class="password-space exit-input">
                                <label for="inputPass" class="col-lg-2 control-label">Пароль:</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" name="password" placeholder="">
                                </div>
                            </div>
                            <div class="birthday-space exit-input">
                                <label for="inputBirthday" class="col-lg-2 control-label">Дата рождения:</label>
                                <div class="col-lg-10">
                                    <input type="date" class="form-control" name="birthday" placeholder="">
                                </div>
                            </div>
                            
                        </div>            
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" name='edit' class="btn btn-danger">Подтвердить</button>
                            </div>
                        </div>
                    </fieldset>
                   
                </form>
            </div>
        </div>